import java.util.Date;

public class CurrentDay {

    private Date date;
    private Double temperature;
    private Double humidity;
    private Double windSpeed;
    private Double windDirection;


    public CurrentDay(Date date, Double temperature, Double humidity, Double windSpeed, Double windDirection) {
        this.date = date;
        this.temperature = temperature;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }


    public Date getDate() {
        return date;
    }

    public Double getTemperature() {
        return temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public Double getWindDirection() {
        return windDirection;
    }


    @Override
    public String toString() {
        return "CurrentDay{" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                ", windDirection=" + windDirection +
                '}';
    }
}
